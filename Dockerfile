#init a base image Alpine linux distro
FROM  python:3.6.1-alpine
 
#define the present working directory
WORKDIR /deneme


#copy the contents into the working dir
ADD  . /deneme

CMD python app.py
